# Nurina

Nurina is a URI parser for Elixir. It was written for fun and as a learning exercise, so it's probably not terribly
useful. It uses pattern matching instead of regex to parse the given URI, and attempts to follow RFC 3986.
