defmodule Nurina.Speedtest do

    def run(_, iterations, _) when iterations == 0, do: nil

    def run(url, iterations, :nurina) do
        Nurina.parse(url)
        run url, iterations - 1, :nurina
    end

    def run(url, iterations, :uri) do
        URI.parse(url)
        run url, iterations - 1, :uri
    end

end
