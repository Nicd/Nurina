defmodule NurinaTest do
  use ExUnit.Case

  test :parse_http do
    assert %Nurina.Info{scheme: "http", host: "foo.com", path: "/path/to/something",
                    query: "foo=bar&bar=foo", fragment: "fragment", port: 80,
                    authority: "foo.com", userinfo: nil,
                    hier: "//foo.com/path/to/something", valid: true} ==
                Nurina.parse("http://foo.com/path/to/something?foo=bar&bar=foo#fragment")
  end

  test :parse_https do
    assert %Nurina.Info{scheme: "https", host: "foo.com", authority: "foo.com",
                    query: nil, fragment: nil, port: 443, path: nil, userinfo: nil,
                    hier: "//foo.com", valid: true} ==
                 Nurina.parse("https://foo.com")
  end

  test :parse_file do
    assert %Nurina.Info{scheme: "file", host: nil, path: "/foo/bar/baz", userinfo: nil,
                    query: nil, fragment: nil, port: nil, authority: nil,
                    hier: "///foo/bar/baz", valid: true} ==
                 Nurina.parse("file:///foo/bar/baz")
  end

  test :parse_ftp do
    assert %Nurina.Info{scheme: "ftp", host: "private.ftp-servers.example.com",
                    userinfo: "user001:secretpassword", authority: "user001:secretpassword@private.ftp-servers.example.com",
                    path: "/mydirectory/myfile.txt", query: nil, fragment: nil,
                    port: 21,
                    hier: "//user001:secretpassword@private.ftp-servers.example.com/mydirectory/myfile.txt", valid: true} ==
                 Nurina.parse("ftp://user001:secretpassword@private.ftp-servers.example.com/mydirectory/myfile.txt")
  end

  test :parse_sftp do
    assert %Nurina.Info{scheme: "sftp", host: "private.ftp-servers.example.com",
                    userinfo: "user001:secretpassword", authority: "user001:secretpassword@private.ftp-servers.example.com",
                    path: "/mydirectory/myfile.txt", query: nil, fragment: nil,
                    port: 22,
                    hier: "//user001:secretpassword@private.ftp-servers.example.com/mydirectory/myfile.txt", valid: true} ==
                 Nurina.parse("sftp://user001:secretpassword@private.ftp-servers.example.com/mydirectory/myfile.txt")
  end

  test :parse_tftp do
    assert %Nurina.Info{scheme: "tftp", host: "private.ftp-servers.example.com",
                    userinfo: "user001:secretpassword", authority: "user001:secretpassword@private.ftp-servers.example.com",
                    path: "/mydirectory/myfile.txt", query: nil, fragment: nil, port: 69,
                    hier: "//user001:secretpassword@private.ftp-servers.example.com/mydirectory/myfile.txt", valid: true} ==
                 Nurina.parse("tftp://user001:secretpassword@private.ftp-servers.example.com/mydirectory/myfile.txt")
  end


  test :parse_ldap do
    assert %Nurina.Info{scheme: "ldap", host: nil, authority: nil, userinfo: nil,
                    path: "/dc=example,dc=com", query: "?sub?(givenName=John)",
                    fragment: nil, port: 389,
                    hier: "///dc=example,dc=com", valid: true} ==
                 Nurina.parse("ldap:///dc=example,dc=com??sub?(givenName=John)")
    assert %Nurina.Info{scheme: "ldap", host: "ldap.example.com", authority: "ldap.example.com",
                    userinfo: nil, path: "/cn=John%20Doe,dc=example,dc=com", fragment: nil,
                    port: 389, query: nil,
                    hier: "//ldap.example.com/cn=John%20Doe,dc=example,dc=com", valid: true} ==
                 Nurina.parse("ldap://ldap.example.com/cn=John%20Doe,dc=example,dc=com")
  end

  test :parse_mailto do
    assert %Nurina.Info{scheme: "mailto", host: nil, authority: nil, userinfo: nil,
                    path: "foo@foo.com", query: nil, fragment: nil, port: nil, hier: "foo@foo.com", valid: true} ==
                  Nurina.parse("mailto:foo@foo.com")
  end

  test :parse_splits_authority do
    assert %Nurina.Info{scheme: "http", host: "foo.com", path: nil,
                    query: nil, fragment: nil, port: 4444,
                    authority: "foo:bar@foo.com:4444",
                    userinfo: "foo:bar",
                    hier: "//foo:bar@foo.com:4444", valid: true} ==
                 Nurina.parse("http://foo:bar@foo.com:4444")
    assert %Nurina.Info{scheme: "https", host: "foo.com", path: nil,
                    query: nil, fragment: nil, port: 443,
                    authority: "foo:bar@foo.com", userinfo: "foo:bar",
                    hier: "//foo:bar@foo.com", valid: true} ==
                 Nurina.parse("https://foo:bar@foo.com")
    assert %Nurina.Info{scheme: "http", host: "foo.com", path: nil,
                    query: nil, fragment: nil, port: 4444,
                    authority: "foo.com:4444",
                    userinfo: nil,
                    hier: "//foo.com:4444", valid: true} ==
                 Nurina.parse("http://foo.com:4444")
  end

  test :parse_bad_uris do
    %Nurina.Info{valid: false} = Nurina.parse("https??@?F?@#>F//23/")
    %Nurina.Info{valid: false} = Nurina.parse("")
    %Nurina.Info{valid: false} = Nurina.parse(":https")
    %Nurina.Info{valid: false} = Nurina.parse("https")
    %Nurina.Info{valid: false} = Nurina.parse("http://example.com:what/")
  end

  test :ipv6_addresses do
    addrs = [
      "::",                                      # undefined
      "::1",                                     # loopback
      "1080::8:800:200C:417A",                   # unicast
      "FF01::101",                               # multicast
      "2607:f3f0:2:0:216:3cff:fef0:174a",        # abbreviated
      "2607:f3F0:2:0:216:3cFf:Fef0:174A",        # mixed hex case
      "2051:0db8:2d5a:3521:8313:ffad:1242:8e2e", # complete
      "::00:192.168.10.184"                      # embedded IPv4
    ]

    Enum.each addrs, fn(addr) ->
      simple_uri = Nurina.parse("http://[#{addr}]/")
      assert simple_uri.host == addr

      userinfo_uri = Nurina.parse("http://user:pass@[#{addr}]/")
      assert userinfo_uri.host == addr
      assert userinfo_uri.userinfo == "user:pass"

      port_uri = Nurina.parse("http://[#{addr}]:2222/")
      assert port_uri.host == addr
      assert port_uri.port == 2222
      
      userinfo_port_uri = Nurina.parse("http://user:pass@[#{addr}]:2222/")
      assert userinfo_port_uri.host == addr
      assert userinfo_port_uri.userinfo == "user:pass"
      assert userinfo_port_uri.port == 2222
    end
  end

  test :downcase_scheme do
    assert Nurina.parse("hTtP://google.com").scheme == "http"
  end

  #test :to_string do
  #  assert to_string(Nurina.parse("http://google.com")) == "http://google.com"
  #  assert to_string(Nurina.parse("http://google.com:443")) == "http://google.com:443"
  #  assert to_string(Nurina.parse("https://google.com:443")) == "https://google.com"
  #  assert to_string(Nurina.parse("http://lol:wut@google.com")) == "http://lol:wut@google.com"
  #  assert to_string(Nurina.parse("http://google.com/elixir")) == "http://google.com/elixir"
  #  assert to_string(Nurina.parse("http://google.com?q=lol")) == "http://google.com?q=lol"
  #  assert to_string(Nurina.parse("http://google.com?q=lol#omg")) == "http://google.com?q=lol#omg"
  #end
end
